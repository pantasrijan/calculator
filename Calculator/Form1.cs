﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        private string boxString;
        private float firstNum;
        private int calculate = 0;
        float result;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void btnMultiply_Click(object sender, EventArgs e)
        {
            firstNum = float.Parse(boxInput.Text);
            calculate = 3;
            boxString = "";
            labelHistory.Text = firstNum + " x ";

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            calculate = 0;
            boxString = "";
            boxDisplay("0");
            labelHistory.Text="";
        }

        private void btnPercentage_Click(object sender, EventArgs e)
        {
            firstNum = float.Parse(boxInput.Text);
            calculate = 5;
            boxString = "";
            labelHistory.Text = firstNum + " % ";
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            firstNum = float.Parse(boxInput.Text);
            calculate = 4;
            boxString = "";
            labelHistory.Text = firstNum + " ÷ ";

        }

        private void btnSub_Click(object sender, EventArgs e)
        {


            firstNum = float.Parse(boxInput.Text);
            calculate = 2;
            boxString = "";
            labelHistory.Text = firstNum + " - ";

        }

        private void btn4_Click(object sender, EventArgs e)
        {
            boxString += "4";
            boxDisplay(boxString);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            boxString += "5";
            boxDisplay(boxString);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            boxString += "6";
            boxDisplay(boxString);
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            float convert = -(float.Parse(boxInput.Text));
            boxDisplay(convert.ToString());
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            boxString += "2";
            boxDisplay(boxString);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            boxString += "3";
            boxDisplay(boxString);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            firstNum = float.Parse(boxInput.Text);
            calculate = 1;
            boxString = "";
            labelHistory.Text=firstNum+" + ";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            boxString += "0";
            boxDisplay(boxString);
        }

        private void btnDot_Click(object sender, EventArgs e)
        {
            boxString+=".";
            boxDisplay(boxString);
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            switch (calculate)
            {
                case 1: //add
                    result = firstNum + float.Parse(boxInput.Text);
                    labelHistory.Text = firstNum + " + " + float.Parse(boxInput.Text) + " = ";
                    boxDisplay(result.ToString());
                    boxString = "";
                    break;
                case 2: //subtract
                    result = firstNum - float.Parse(boxInput.Text);
                    labelHistory.Text = firstNum + " - " + float.Parse(boxInput.Text) + " = ";
                    boxDisplay(result.ToString());
                    boxString = "";
                    break;
                case 3: //multiply
                    result = firstNum * float.Parse(boxInput.Text);
                    labelHistory.Text = firstNum + " x " + float.Parse(boxInput.Text) + " = ";
                    boxDisplay(result.ToString());
                    boxString = "";
                    break;
                case 4: //divide
                    result = firstNum / float.Parse(boxInput.Text);
                    labelHistory.Text = firstNum + " ÷ " + float.Parse(boxInput.Text) + " = ";
                    boxDisplay(result.ToString());
                    boxString = "";
                    break;
                case 5: //percentage
                    result = firstNum % float.Parse(boxInput.Text);
                    labelHistory.Text = firstNum + " % " + float.Parse(boxInput.Text) + " = ";
                    boxDisplay(result.ToString());
                    boxString = "";
                    break;
                default:
                    break;
                    

            }
        }

        private void labelHistory_Click(object sender, EventArgs e)
        {

        }

        private void boxInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            boxString +="1";
            boxDisplay(boxString);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            boxString +="7";
            boxDisplay(boxString);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            boxString +="8";
            boxDisplay(boxString);
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            boxString +="9";
            boxDisplay(boxString);
        }
        private void boxDisplay(String display)
        {
            boxInput.Text = display;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
